'use strict';
let firstNumber, secondNumber, operation;
do {
    firstNumber = prompt('Введите первое число')
}
while (isNaN(+firstNumber) || !firstNumber || firstNumber.trim() === '')
do {
    secondNumber = prompt('Введите второе число')
}
while (isNaN(+secondNumber) || !secondNumber || secondNumber.trim() === '')
do {
    operation = prompt('Введите знак операции')
}
while (operation !== '+' && operation !== '-' && operation !== '/' && operation !== '*')

function calc ( firstNumber, secondNumber, operation){
    if (operation === '+'){
        return +firstNumber + +secondNumber
    }
    else if (operation === '-'){
        return firstNumber - secondNumber
    }
    else if (operation === '/'){
        return firstNumber / secondNumber
    }
    else if (operation === '*'){
        return firstNumber * secondNumber
    }
}
console.log(calc(firstNumber, secondNumber, operation))
